import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
import numpy as np
import sys

headers= input().split(",")
data = np.genfromtxt(sys.stdin, dtype=float, delimiter=',', skip_header=0)

iterations = data.shape[0]

plt.rcParams.update({'font.size': 18})
plt.figure(figsize=(10,10))
plt.plot(data)
plt.legend(headers, loc='upper center', bbox_to_anchor=(0.5, 1.1), ncol=3, fancybox=True, shadow=True)
plt.gca().xaxis.set_major_locator(mticker.MultipleLocator(1))
plt.ylim(top=0.55, bottom=0)
plt.xlabel("Iterations")
plt.ylabel("PageRank")
#plt.xticks(np.arange(iterations, np.arange(1, iterations+1)))

# last value as text
for v in data[-1]:
    plt.annotate('%0.4f' % v, xy=(1, v), xytext=(4, 0), xycoords=('axes fraction', 'data'), textcoords='offset points')

plt.show()
