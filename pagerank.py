
damping_factor = 0.85

class Page: 

    def __init__(self, name):
        self.name = name
        self.out_links = []
        self.in_links = []
        self.new_pagerank = 0.0
        self.pagerank = 0.0

    def update_pagerank(self):
        self.pagerank = self.new_pagerank

    def pagerank_as_string(self):
        return "{:.8f}".format(self.pagerank)

    def __str__(self):
        return "{name='" + self.name + "' " + \
            "pr='{:.8f}'".format(self.pagerank) + \
            "new_pr='{:.8f}'".format(self.new_pagerank) + \
            "out='" + str([out_link.name for out_link in self.out_links]) + "' " + \
            "in='" + str([in_link.name for in_link in self.in_links]) + "'}"

def set_incoming_links(pages):
    for page in pages:
        for target_page in page.out_links:
            if page not in target_page.in_links:
                target_page.in_links.append(page)

def init_pageranks_equally(pages):
    rank = 1.0 / len(pages) # 1/N
    for page in pages:
        page.pagerank = rank

def init_pageranks_unequally(pages):
    available_weigth = 1.0
    for page in reversed(pages):
        page.pagerank = available_weigth / 2
        available_weigth = available_weigth / 2
    
    # steal weight from b
    available_weigth += pages[1].pagerank
    pages[1].pagerank = 0

    # first element gets the leftover weight
    pages[0].pagerank += available_weigth

def calculate_pageranks(pages, iterations):
    N = len(pages)
    print(",".join([p.name for p in pages]))
    for _ in range(iterations):
        # print current pagerank
        print(",".join([p.pagerank_as_string() for p in pages]))
        
        # calculate new pagerank
        for page in pages:
            page.new_pagerank = (1.0-damping_factor)/N + damping_factor * sum([neighbour.pagerank / len(neighbour.out_links) for neighbour in page.in_links])

        # set current pagerank to new pagerank
        for page in pages:
            page.update_pagerank()



a = Page("a")
b = Page("b")
c = Page("c")
d = Page("d")
e = Page("e")
f = Page("f")

a.out_links = [b, c]
b.out_links = [e]
c.out_links = [a, b, d]
d.out_links = [b]
e.out_links = [b, d]
f.out_links = [c, d]

pages = [a, b, c, d, e, f]

set_incoming_links(pages)
init_pageranks_equally(pages)
calculate_pageranks(pages, 50)
